#!/bin/bash

timedatectl set-timezone America/Los_Angeles
perl -p -i -e 's/^\#Port 22/Port 2200/; s/^PasswordAuthentication yes/PasswordAuthentication no/; s/^\#PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
groupadd -g 1000 nat
useradd -g 1000 -u 1000 nat
perl -p -i -e 's/(^root\s+ALL\=\(ALL\).*)$/\1\nnat ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
mkdir -m 700 /home/nat/.ssh
install -m 600 /dev/null /home/nat/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDgQdjjMvn9/vIm4gdOy9vJCO+88F2J4PFDieX84ymcsBZZmKag8I9e8cBqcrgZGTcgXIXVN/IbTJR1jBqIqdIQAY3q/PGVeJQdiBFQkMg2Q7aubT04EGKIdyfzEto+IIyQ73/S1AJvFJlRRJeqo00SyUyke4GN2mkhjv9Kv7XF9GIqqndE9xWmvEhEyzG2bt5FPjfXKtu6b/n+plwk+Jxm3OilcUGIBZbGd3xcFgUFDLfp/uAZI9HejbT3gTdnbG65AX2L/eZgT44PND/ujYn3b+lNzjrDnAoPZB5XIAJid5QU9LJEmffGsqWxQFALpr4Fuxys9A6PrVcQ5YeY01E6EbVCU84Pcl/YpUJf5D2q24fv9e3uhl5gzR+8hIUNya4HWU0au7RkPvWgizciRvHYfB0QQIcznyBwbq3/2QTZ/OD7grYeuXEynWZzNTY6vGuYImhRtIS7t5t7OFNSN5g9KDv2V/GoJkAEqGtthDOSP3U5W4VmbSrQjLfDQ9fijBKOXXWXf4rBqD9vyARexW8MEEmoz4COFKYevOqr3F74eAmt3SHqMD3/xW7eGeV40uPTkyX8KX2qJ1O9+125nbHRniF5QslJBto5P4ICB7ttRe1HvcxJdnVLRfMdCN8pLIgDIjNn10Pm7TDdBOkrj6uvDcgGWmmpMFaEDobqT+IufQ== nmclean@gnatbook
" >> /home/nat/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9W2xwAuUZQwSWxST8+lVUQZSR0z1C0Z0iXF1OjPn0U8SomNwVRpnizgP45IaeLQUId7NcrS3jZoEGLG9N2rFzHvFbb+MYfBsh5C02UjJcLGJKgvectMc8dVMhhc4FMsfmTE/bvwnOXcfnESOwS1CMAF5cVRRLrpVnAb3RyAiA+KF0LHQxffQedAHXiP8/FnlTxrRQTkWWIy5sv7zB2EuxEsBffRdZ6ScCip1GeYIx8N7/pAzQNECpdH1J2nL6fz/AuY4OEYKLBfzU5FlBchU6dPdxhh+xe79QNPyt4TEznz+mloHpBGQgRQq2APuGFotk6P1MUjAFW04Z2WhEC1Bh root@8522-frqc.accessdomain.com
" >> /home/nat/.ssh/authorized_keys
chown -R nat:nat /home/nat
systemctl restart sshd
